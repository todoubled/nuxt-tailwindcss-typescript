# carbonwp-nuxt

> Nuxt.js - TailwindCSS - TypeScript starter project

## SASS-SMACSS
- You can find the styles and tailwind css applied within the layouts/default.vue path.  

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
